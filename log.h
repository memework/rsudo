#ifndef __LOG_H__
#define __LOG_H__

#define LOG_DEBUG(m, s) printf("[DEBUG] [%s] %s\n", m, s)
#define LOG_INFO(m, s) printf("[INFO] [%s] %s\n", m, s)
#define LOG_WARN(m, s) printf("[WARN] [%s] %s\n", m, s)
#define LOG_ERROR(m, s) printf("[ERROR] [%s] %s\n", m, s)

#endif
