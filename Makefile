INSTALL_PREFIX = /usr/local/bin
CC = gcc

all: rsudo setuid

clang-format: rsudo.c
	clang-format -i rsudo.c

setuid: rsudo
	chown root:root rsudo
	chmod 4711 rsudo

.PHONY: all setuid install clang-format

rsudo: rsudo.c
	$(CC) -o ./rsudo rsudo.c

install: rsudo
	install -Dm4711 rsudo $(INSTALL_PREFIX)/rsudo

